var slideIndex = 1;
showDivs(slideIndex);
    
function plusDivs(n) {
    showDivs(slideIndex += n);
}
    
function currentDiv(n) {
    showDivs(slideIndex = n);
}
    
function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("slides");
    var dots = document.getElementsByClassName("demo");
    if (n > x.length) {slideIndex = 1}
    if (n < 1) {slideIndex = x.length}
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";  
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" w3-white", "");
    }
    x[slideIndex-1].style.display = "block";  
    dots[slideIndex-1].className += " w3-white";
}

carousel();
function carousel() {
    plusDivs(1);
    setTimeout(carousel, 4000);
}
    
$('#contact-form-send').submit(function(e) {
    e.preventDefault();
    var name = $("#name").val();
    var email = $("#email").val();
    var phone = $("#mobile").val();
    var message = $("#message").val();

    var text = "name: "+name+", email: "+email+", contact: "+phone+", message: "+message;
    var url ="PHP/save.php";
    $.post(url, { data: text }, function(data){
        console.log('response from the callback function: '+ data);
        swal("Success!", "Message sent successfully!", "success");
    }).fail(function(jqXHR){
        console.log(jqXHR.status +' '+jqXHR.statusText+ ' $.post failed!');
        swal("Failed!", "Message not sent!", "error");
  });   
});